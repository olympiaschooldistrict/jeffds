module bitbucket.org/olympiaschooldistrict/jeffds

go 1.12

require (
	cloud.google.com/go v0.40.0
	github.com/hashicorp/golang-lru v0.5.1
)
