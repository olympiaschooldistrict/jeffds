package jeffds

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"cloud.google.com/go/datastore"
	lru "github.com/hashicorp/golang-lru"
)

const (
	//Default Datastore Kind
	defaultKind = "JeffSession1"

	//Default LRU cache size
	defaultCacheSize = 3000
)

// New creates a new Store
func New(projectID string) *JeffDS {
	cache, err := lru.New(defaultCacheSize)
	if err != nil {
		return nil
	}
	client, err := datastore.NewClient(context.Background(), projectID)
	if err != nil {
		return nil
	}

	return &JeffDS{
		projectID: projectID,
		kind:      defaultKind,
		cacheSize: defaultCacheSize,
		cache:     cache,
		client:    client,
	}
}

// SetKind lets you change the datastore kind from the default
func (j *JeffDS) SetKind(kind string) error {
	if kind == "" {
		return fmt.Errorf("kind cannot be blank")
	}
	if strings.HasPrefix(kind, "__") {
		return fmt.Errorf("kinds starting with __ are reserved")
	}
	j.kind = kind
	return nil
}

// SetCacheSize lets you change the LRU cachesize kind from the default
func (j *JeffDS) SetCacheSize(size int) (err error) {
	if size <= 0 {
		return fmt.Errorf("size must be greater than 0")
	}
	j.cacheSize = size
	j.cache, err = lru.New(j.cacheSize)
	return err
}

// Store persists the session in the backend with the given expiration
// Implementation must return value exactly as it is received.
// Value will be given as...
func (j *JeffDS) Store(ctx context.Context, key, value []byte, exp time.Time) (err error) {
	i := &Item{value, exp}
	// datastore.IncompleteKey(j.kind)
	k := datastore.NameKey(j.kind, string(key), nil)
	_, err = j.client.Put(ctx, k, i)
	if err != nil {
		return fmt.Errorf("couldn't put %s to %s with %v exp and kind %s: %v", value, key, exp, j.kind, err)
	}
	j.cache.Add(CKey(string(key)), i)
	return
}

// Fetch retrieves the session from the backend.  If err != nil or
// value == nil, then it's assumed that the session is invalid and Jeff
// will redirect.  Expired sessions must return nil error and nil value.
// Unknown (not found) sessions must return nil error and nil value.
func (j *JeffDS) Fetch(ctx context.Context, key []byte) (value []byte, err error) {
	iPerhaps, ok := j.cache.Get(CKey(string(key)))
	if ok {
		i, ok := iPerhaps.(*Item)
		if ok {
			if i.Exp.Before(time.Now()) {
				j.cache.Remove(CKey(string(key)))
				return nil, nil
			}
			return i.Value, nil
		}
		j.cache.Remove(CKey(string(key)))
	}
	k := datastore.NameKey(j.kind, string(key), nil)
	i := &Item{}
	err = j.client.Get(ctx, k, i)
	if err == nil {
		if i.Exp.Before(time.Now()) {
			return nil, nil
		}
		value = i.Value
	}
	if err != nil {
		err = fmt.Errorf("couldn't get %s with kind %s: %v", key, j.kind, err)
		return nil, nil
	}
	j.cache.Add(CKey(string(key)), i)
	return
}

// Delete removes the session given by key from the store. Errors are
// bubbled up to the caller.  Delete should not return an error on expired
// or missing keys.
func (j *JeffDS) Delete(ctx context.Context, key []byte) (err error) {
	k := datastore.NameKey(j.kind, string(key), nil)
	err = j.client.Delete(ctx, k)
	if err != nil {
		log.Printf("Couldn't delete for key %s: %#v", string(key), err)
	}
	j.cache.Remove(CKey(string(key)))
	return
}

// JeffDS is a datastore storage for jeff
type JeffDS struct {
	projectID string
	kind      string
	cacheSize int
	cache     *lru.Cache
	client    *datastore.Client
}

// Item is an individual session
type Item struct {
	Value []byte    `datastore:"value,noindex"`
	Exp   time.Time `datastore:"exp,noindex"`
}

// CKey is used for more typesafety
type CKey string
