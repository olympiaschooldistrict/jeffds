package jeffds

import (
	"testing"
)

func TestJeffDS_SetKind(t *testing.T) {
	type fields struct {
		// projectID string
		kind string
		// cacheSize int
		// cache     *lru.Cache
		// client    *datastore.Client
	}
	type args struct {
		kind string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{"empty", fields{"not"}, args{""}, true},
		{"expectedCase", fields{"newKind"}, args{"newKind"}, false},
		{"reserved", fields{"__newKind"}, args{"__newKind"}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := &JeffDS{
				// projectID: tt.fields.projectID,
				kind: tt.fields.kind,
				// cacheSize: tt.fields.cacheSize,
				// cache:     tt.fields.cache,
				// client:    tt.fields.client,
			}
			if err := j.SetKind(tt.args.kind); (err != nil) != tt.wantErr {
				t.Errorf("JeffDS.SetKind() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestJeffDS_SetCacheSize(t *testing.T) {
	type fields struct {
		// projectID string
		// kind      string
		cacheSize int
		// cache     *lru.Cache
		// client    *datastore.Client
	}
	type args struct {
		size int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{"negative", fields{defaultCacheSize}, args{-1}, true},
		{"zero", fields{defaultCacheSize}, args{0}, true},
		{"positive", fields{74}, args{74}, false},

		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := &JeffDS{
				// projectID: tt.fields.projectID,
				// kind:      tt.fields.kind,
				cacheSize: tt.fields.cacheSize,
				// cache:     tt.fields.cache,
				// client:    tt.fields.client,
			}
			if err := j.SetCacheSize(tt.args.size); (err != nil) != tt.wantErr {
				t.Errorf("JeffDS.SetCacheSize() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
